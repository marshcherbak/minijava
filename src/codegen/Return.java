package codegen;

public class Return extends Statement {
    private Expression expression;
    public Expression getExpression(){
        return expression;
    }

    public Return(Expression expression) {
        super();
        this.expression = expression;
    }

    public void accept(ProgramVisitor visitor){
        visitor.visit(this);
    }
}

package codegen;

public class Unary extends Expression {
    private Unop operator;
    private Expression operand;

    public Unop getOperator() {
        return operator;
    }

    public Expression getOperand() {
        return operand;
    }

    public Unary(Unop operator, Expression operand) {
        super();
        this.operator = operator;
        this.operand = operand;
    }

    public void accept(ProgramVisitor visitor){
        visitor.visit(this);
    }
}

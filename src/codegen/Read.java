package codegen;

public class Read extends Expression {
    public Read() {
        super();
    }

    public void accept(ProgramVisitor visitor){
        visitor.visit(this);
    }
}

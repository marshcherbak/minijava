package codegen;

import asm.*;

public class CodegenTest {
    public static void testExamplesClass(){
        System.out.println("Programs from examples in classwork:" + Terminal.NEWLINE + Terminal.NEWLINE);
        Function main = new Function("main",
                new String[]{},
                new Declaration[]{},
                new Statement[]{
                        new ExpressionStatement(new Write(
                                new Binary(new Read(), Binop.Plus, new Read())
                        )),
                        new Return(new Number(0))
                });

        Program p = new Program(new Function[]{main});
        test(p);
        main = new Function("main",
                new String[]{},
                new Declaration[]{
                        new Declaration(new String[]{"i", "n", "result"})},
                new Statement[]{
                        new Assignment("n", new Read()),
                        new Assignment("result", new Number(0)),
                        new Assignment("i", new Number(1)),
                        new While(
                                new Comparison(new Variable("i"), Comp.NotEquals, new Variable("n")),
                                new Composite (new Statement[]
                                        {
                                                new Assignment("result",
                                                        new Binary(new Variable("result"), Binop.Plus, new Variable("i"))),
                                                new Assignment("i", new Binary(new Variable("i"),
                                                        Binop.Plus, new Number(1)))
                                        }),
                                false),
                        new ExpressionStatement(new Write(new Variable("result"))),
                        new Return(new Number(0))}
        );

        p = new Program(new Function[]{main});
        test(p);

        main = new Function("main",
                new String[] {},
                new Declaration[] {},
                new Statement[] {
                        new ExpressionStatement(new Write(new Call("sum", new Expression[]{
                                new Read(),
                                new Read()
                        }))),
                        new Return(new Number(0))
                });
        Function sum = new Function("sum",
                new String[] {"a", "b"},
                new Declaration[] {},
                new Statement[]{
                        new Return(new Binary(new Variable("a"), Binop.Plus, new
                                Variable("b")))
                });

        Program prog = new Program(new Function[] { sum, main });
        test(prog);

    }

    public static void testExamplesHome(){
        System.out.println("Testing programs from homework:"+Terminal.NEWLINE+Terminal.NEWLINE);
        Function fak = new Function("fak",
                new String[]{"n"},
                new Declaration[]{},
                new Statement[]{
                        new IfThenElse(new Comparison(new Variable("n"), Comp.Equals, new Number(1)), new
                                Return(new Number(1)), null),
                        new Return(new Binary(new Variable("n"), Binop.MultiplicationOperator,
                                new Call("fak", new Expression[]{
                                        new Binary(new Variable("n"), Binop.Minus, new Number(1))
                                })))

                });

        Function main = new Function("main",
                new String[]{},
                new Declaration[]{},
                new Statement[]{
                        new ExpressionStatement(new Write(new Call("fak", new Expression[]{new Read()}))),
                        new Return(new Number(0))
                });
        Program test = new Program(new Function[]{fak, main});
        test(test);

        Function ggt = new Function("ggt",
                new String[]{"a", "b"},
                new Declaration[]{new Declaration(new String[]{"temp"})},
                new Statement[]{
                        new IfThenElse(new Comparison(new Variable("b"),
                                Comp.GreaterEqual, new Variable("a")),
                                new Composite(new Statement[]{
                                        new Assignment("temp", new Variable("b")),
                                        new Assignment("b", new Variable("a")),
                                        new Assignment("a", new Variable("temp"))
                                }),
                                null),
                        new While(new Comparison(new Variable("b"), Comp.NotEquals, new Number(0)),
                                new Composite(new Statement[]{
                                        new Assignment("temp", new Variable("b")),
                                        new Assignment("b", new Binary(new Variable("a"), Binop.Modulo, new Variable("b"))),
                                        new Assignment("a", new Variable("temp"))
                                }), true),
                        new Return(new Variable("a"))

                });

        main = new Function("main",
                new String[]{},
                new Declaration[]{},
                new Statement[]{
                        new ExpressionStatement(new Write(new Call("ggt", new Expression[]{
                                new Read(), new Read()
                        }))),
                        new Return(new Number(0))
                });

        Program test2 = new Program(new Function[]{ggt, main});
        test(test2);

    }

    public static void testExamplesOwn(){
        System.out.println("Testing own examples:"+Terminal.NEWLINE+Terminal.NEWLINE);
        Function main = new Function("main",
                new String[] {},
                new Declaration[] {new Declaration(new String[]{"a", "b"})},
                new Statement[] {
                        new Assignment("a", new Read()),
                        new Assignment("b", new Read()),
                        new Assignment("b", new Call("doublePow",
                                new Expression[]{
                                        new Unary(Unop.Minus, new Call("sum", new Expression[]{
                                                new Variable("a"),
                                                new Variable("b")
                                        })),
                                        new Number(2)
                                })),
                        new ExpressionStatement(new Write(new Variable("b"))),
                        new Return( new Number(0))
                });

        Function sum = new Function("sum",
                new String[] {"a", "b"},
                new Declaration[] {},
                new Statement[] {
                        new Return(new Binary(
                                new Variable("a"),
                                Binop.Plus,
                                new Variable("b"))
                        )
                });
        /*!!!doesn't work as a normal pow function, with */
        Function pow = new Function("doublePow",
                new String[] {"a", "b"},
                new Declaration[] {new Declaration(new String[]{"i"})},
                new Statement[] {
                        new IfThenElse(
                                new BinaryCondition(
                                        new Comparison(new Variable("a"), Comp.Equals, new Number(0)),
                                        Bbinop.Or,
                                        new Comparison(new Variable("b"), Comp.Equals, new Number(0))),
                                new Return(new Number(1)),
                                null),
                        new IfThenElse(new Comparison(new Variable("a"),Comp.Equals,new Number(1)), new Return(new Variable("a")),null),
                        new Assignment("i", new Number(1)),
                        new While(
                                new Comparison(new Variable("i"), Comp.NotEquals, new Variable("b")),
                                new Composite(new Statement[]{
                                        new Assignment("a", new Binary(new Variable("a"),Binop.MultiplicationOperator, new Variable("a"))),
                                        new Assignment("i", new Binary(new Variable("i"),Binop.Plus,new Number(1)))
                                }),true),
                        new Return(new Variable("a"))
                });
        Program test3 = new Program(new Function[] {main, sum, pow});
        test(test3);

        sum = new Function("sum",
                new String[] {"a", "b"},
                new Declaration[] {},
                new Statement[] {
                        new Return(new Binary(
                                new Call("doublePow", new Expression[]{
                                        new Variable("a"),
                                        new Number(2)
                                }),
                                Binop.Plus,
                                new Variable("b"))
                        )
                });

        test3 = new Program(new Function[] {main, pow, sum});
        test(test3);
    }

    public static void testInvalidExamples(){

        /* not declared variable */
        try {
            Function main = new Function("main",
                    new String[]{},
                    new Declaration[]{
                            new Declaration(new String[]{"i", "n"})},
                    new Statement[]{
                            new Assignment("n", new Read()),
                            new Assignment("result", new Number(0)),
                            new Assignment("i", new Number(1)),
                            new While(
                                    new Comparison(new Variable("i"), Comp.NotEquals, new Variable("n")),
                                    new Composite(new Statement[]
                                            {
                                                    new Assignment("result",
                                                            new Binary(new Variable("result"), Binop.Plus, new Variable("i"))),
                                                    new Assignment("i", new Binary(new Variable("i"),
                                                            Binop.Plus, new Number(1)))
                                            }),
                                    false),
                            new ExpressionStatement(new Write(new Variable("result"))),
                            new Return(new Number(0))}
            );

        Program p = new Program(new Function[]{main});
        test(p);
    }
        catch (Exception e){
            System.out.println(e);
        }
        /* no return */
        try {
            Function main = new Function("main",
                    new String[]{},
                    new Declaration[]{},
                    new Statement[]{
                            new ExpressionStatement(new Write(new Call("sum", new Expression[]{
                                    new Read(),
                                    new Read()
                            }))),
                            new Return(new Number(0))
                    });
            Function sum = new Function("sum",
                    new String[]{"a", "b"},
                    new Declaration[]{},
                    new Statement[]{
                            /*new Return(new Binary(new Variable("a"), Binop.Plus, new
                                    Variable("b")))*/
                    });

            Program prog = new Program(new Function[]{sum, main});
            test(prog);
        }
        catch (Exception e){
            System.out.println(e);
        }
        /* no main*/
        try {
            Function sum = new Function("sum",
                    new String[]{"a", "b"},
                    new Declaration[]{},
                    new Statement[]{
                            new Return(new Binary(new Variable("a"), Binop.Plus, new
                                    Variable("b")))
                    });
            Program prog = new Program(new Function[]{sum});
            test(prog);
        }
        catch (Exception e){
            System.out.println(e);
        }
        /*unknown function called*/
        try {
            Function main = new Function("main",
                    new String[]{},
                    new Declaration[]{},
                    new Statement[]{
                            new ExpressionStatement(new Write(new Call("fak", new Expression[]{
                                    new Read(),
                                    new Read()
                            }))),
                            new Return(new Number(0))
                    });
            Program prog = new Program(new Function[]{main});
            test(prog);
        }
        catch (Exception e){
            System.out.println(e);
        }
    }



    public static void test(Program testProgram){
        FormatVisitor fv = new FormatVisitor();
        testProgram.accept(fv);
        System.out.println(fv.getFormattedCode());

        CodeGenerationVisitor cgv = new CodeGenerationVisitor();
        testProgram.accept(cgv);
        AsmFormatVisitor asmf = new AsmFormatVisitor(cgv.getProgram());
        System.out.println(asmf.getFormattedCode());

        Interpreter in = new Interpreter(cgv.getProgram());
        in.execute();

        System.out.println(Terminal.NEWLINE + Terminal.NEWLINE);
    }
    public static void main(String[] args) {

        do{
            String s = "1. Test examples from class"+
                    Terminal.NEWLINE + "2. Test examples from homework" +
                    Terminal.NEWLINE + "3. Test own examples" +
                    Terminal.NEWLINE + "4. Test invalid examples that will throw exceptions" +
                    Terminal.NEWLINE + "5. Exit" + Terminal.NEWLINE;
            s+= "Choose number: ";
            int ask = Terminal.askInt(s);
            switch (ask){
                case 1:
                    testExamplesClass();
                    break;
                case 2:
                    testExamplesHome();
                    break;
                case 3:
                    testExamplesOwn();
                    break;
                case 4:
                    testInvalidExamples();
                    break;
                case 5:
                    return;
                default:
                    System.out.println("Invalid number.");
                    break;
            }

        }while(true);
    }
}

package codegen;

public interface ProgramVisitor {
    void visit(Assignment assignment);
    void visit(Binary binary);
    void visit(BinaryCondition binaryCondition);
    void visit(Call call);
    void visit(Comparison comparison);
    void visit(Composite composite);
    void visit(Declaration declaration);
    void visit(ExpressionStatement expressionStatement);
    void visit(False ff);
    void visit(Function function);
    void visit(IfThenElse ifThenElse);
    void visit(Number number);
    void visit(Program program);
    void visit(Read read);
    void visit(Return rreturn);
    void visit(True tt);
    void visit(Unary unary);
    void visit(UnaryCondition unaryCondition);
    void visit(Variable variable);
    void visit(While ww);
    void visit(Write write);
}

package codegen;

import asm.Terminal;

public class NoReturnException extends CodeGenException{
    String s;
    public NoReturnException(String s){
        super();
        this.s = s;
    }

    public String toString(){
        return super.toString() + Terminal.NEWLINE + "Function " + s + " has no return statement." + Terminal.NEWLINE;
    }
}

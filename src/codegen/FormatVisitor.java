package codegen;

import asm.Terminal;

public class FormatVisitor implements ProgramVisitor {

    private String formattedCode;
    private final String tab = "    ";
    private int nTab;
    public FormatVisitor(){
        formattedCode = "";
    }
    public String getFormattedCode() {
        return formattedCode;
    }
    private boolean funR = false;
    private void tabs(int n){
        for(int i = 0; i < n; i++)
            formattedCode+=tab;
    }
    @Override
    public void visit(Assignment assignment){
        formattedCode+=assignment.getName()+" = ";
        assignment.getExpression().accept(this);
    }
    @Override
    public void visit(Binary binary){
        binary.getLhs().accept(this);
        switch (binary.getOperator()){
            case Minus:
                formattedCode += " - ";
                break;
            case Plus:
                formattedCode+= " + ";
                break;
            case MultiplicationOperator:
                formattedCode+= " * ";
                break;
            case DivisionOperator:
                formattedCode+= " / ";
                break;
            case Modulo:
                formattedCode += " % ";
                break;
        }
        binary.getRhs().accept(this);
    }
    @Override
    public void visit(BinaryCondition binaryCondition){
        binaryCondition.getLhs().accept(this);
        switch (binaryCondition.getOperator()){
            case And:
                formattedCode+=" && ";
                break;
            case Or:
                formattedCode+=" || ";
                break;
        }
        binaryCondition.getRhs().accept(this);
    }
    @Override
    public void visit(Call call){
        formattedCode+=call.getFunctionName() + "(";
        for(int i = 0; i < call.getArguments().length-1; i++){
            call.getArguments()[i].accept(this);
            formattedCode+=", ";
        }
        call.getArguments()[call.getArguments().length-1].accept(this);
        formattedCode+=")";
    }
    @Override
    public void visit(Comparison comparison){
        comparison.getLhs().accept(this);
        switch(comparison.getOperator()){
            case Less:
                formattedCode+=" < ";
                break;
            case Equals:
                formattedCode+= " == ";
                break;
            case Greater:
                formattedCode+=" > ";
                break;
            case LessEqual:
                formattedCode+=" <= ";
                break;
            case NotEquals:
                formattedCode+=" != ";
                break;
            case GreaterEqual:
                formattedCode+=" >= ";
                break;
        }
        comparison.getRhs().accept(this);
    }
    @Override
    public void visit(Composite composite){
        for(Statement s : composite.getStatements()){
            tabs(nTab);
            s.accept(this);
            formattedCode+=";"+Terminal.NEWLINE;
        }
    }
    @Override
    public void visit(Declaration declaration){
        formattedCode+="int";
        int i;
        for(i = 0; i < declaration.getNames().length-1; i++){
            formattedCode+=" " + declaration.getNames()[i] + ",";
        }
        formattedCode+=" "+ declaration.getNames()[i];
    }
    @Override
    public  void visit(ExpressionStatement expressionStatement){
        expressionStatement.getExpression().accept(this);
    }
    @Override
    public void visit(False ff){
        formattedCode+="false";
    }
    @Override
    public void visit(Function function){
        formattedCode+="int " + function.getName() + " (";
        for(int i = 0; i < function.getParameters().length - 1; i++){
            formattedCode+="int " + function.getParameters()[i] + ", ";
        }
        if(function.getParameters().length>=1)
            formattedCode+="int " + function.getParameters()[function.getParameters().length-1] + ")";
        else
            formattedCode+=")";
        formattedCode+=" {"+Terminal.NEWLINE;
        //nTab++;
        for (Declaration d : function.getDeclarations()) {
            nTab++;
            tabs(nTab);
            visit(d);
            formattedCode+=";"+Terminal.NEWLINE;
            nTab--;
        }
        for (Statement s : function.getStatements()) {
            funR = true;
            nTab++;
            tabs(nTab);
            s.accept(this);
            if(!(s instanceof IfThenElse || s instanceof While || s instanceof Composite))
                formattedCode+=";";
            formattedCode+=Terminal.NEWLINE;
            nTab--;
        }
        //nTab--;
        tabs(nTab);
        formattedCode+="}";
    }
    @Override
    public void visit(IfThenElse ifThenElse){
        funR = false;
        formattedCode+="if(";
        ifThenElse.getCond().accept(this);
        formattedCode+=") {"+Terminal.NEWLINE;
        nTab++;
        ifThenElse.getThenBranch().accept(this);
        if(!(ifThenElse.getThenBranch() instanceof Composite))
            formattedCode+=";"+Terminal.NEWLINE;
        nTab--;
        tabs(nTab);
        formattedCode+="}";
        if(ifThenElse.getElseBranch()!=null){
            nTab++;
            formattedCode+="else {"+Terminal.NEWLINE;
            tabs(nTab);
            ifThenElse.getElseBranch().accept(this);
            if(!(ifThenElse.getElseBranch() instanceof Composite)){
                formattedCode+=";" + Terminal.NEWLINE;
            }
            nTab--;
            tabs(nTab);
            formattedCode+="}";
        }
    }
    @Override
    public void visit(Number number){
        formattedCode+=number.getValue();
    }
    @Override
    public void visit(Program program){
        for(Function f : program.getFunctions()){
            f.accept(this);
            formattedCode+=Terminal.NEWLINE+Terminal.NEWLINE;
        }
    }
    @Override
    public void visit(Read read){
        formattedCode+="read()";
    }
    @Override
    public void visit(Return rreturn){
        if(!funR)
            tabs(nTab);
        formattedCode+="return ";
        rreturn.getExpression().accept(this);
    }
    @Override
    public void visit(True tt){
        formattedCode+="true";
    }
    @Override
    public void visit(Unary unary){
        switch(unary.getOperator()){
            case Minus:
                formattedCode+="-";
                break;
        }
        unary.getOperand().accept(this);
    }
    @Override
    public void visit(UnaryCondition unaryCondition){
        switch (unaryCondition.getOperator()){
            case Not:
                formattedCode+="!";
                break;
        }
        unaryCondition.getOperand().accept(this);
    }
    @Override
    public void visit(Variable variable){
        formattedCode+=variable.getName();
    }
    @Override
    public void visit(While ww){
        funR = false;
        if(ww.isDoWhile()){
            formattedCode += "do {"+Terminal.NEWLINE;
            nTab++;
            ww.getBody().accept(this);
            nTab--;
            tabs(nTab);
            formattedCode += "} while (";
            ww.getCond().accept(this);
            formattedCode += ");";
        }
        else {
            formattedCode += "while(";
            ww.getCond().accept(this);
            formattedCode += "){"+Terminal.NEWLINE;
            nTab++;
            ww.getBody().accept(this);
            nTab--;
            tabs(nTab);
            formattedCode += "}";
        }
    }
    @Override
    public void visit(Write write){
        formattedCode+="write(";
        write.getExpression().accept(this);
        formattedCode+=")";
    }
}

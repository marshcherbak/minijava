package codegen;

public class UnaryCondition extends Condition{
    private Bunop operator;
    private Condition operand;

    public Bunop getOperator() {
        return operator;
    }

    public Condition getOperand() {
        return operand;
    }

    public UnaryCondition(Bunop operator, Condition operand) {
        super();
        this.operator = operator;
        this.operand = operand;
    }

    public void accept(ProgramVisitor visitor){
        visitor.visit(this);
    }
}

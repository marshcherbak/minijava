package codegen;

public class ExpressionStatement extends Statement {
    private Expression expression;

    public Expression getExpression() {
        return expression;
    }

    public ExpressionStatement(Expression expression){
        super();
        this.expression = expression;
    }

    public void accept(ProgramVisitor visitor){
        visitor.visit(this);
    }
}

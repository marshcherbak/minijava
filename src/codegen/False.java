package codegen;

public class False extends Condition {
    public False(){
        super();
    }

    public void accept(ProgramVisitor visitor){
        visitor.visit(this);
    }
}

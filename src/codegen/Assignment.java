package codegen;

public class Assignment extends Statement{
    private String name;
    private Expression expression;

    public String getName(){
        return name;
    }
    public Expression getExpression(){
        return expression;
    }

    public Assignment(String name, Expression expression){
        super();
        this.name = name;
        this.expression = expression;
    }

    public void accept(ProgramVisitor visitor){
        visitor.visit(this);
    }

}

package codegen;

import asm.*;
import java.util.*;
import java.util.stream.Stream;

public class CodeGenerationVisitor implements ProgramVisitor {
    private Instruction[] program;
    private Vector<Instruction> v; //instructions
    private int placeHolder, branchHolder;
    private int framePointer = -1;
    private Function currentF = null; //current function being writter
    private ArrayList<Function> pFunctions; //all functions in program
    private ArrayList<String> currentVars; //variables (params + local) of the current function
    private TreeMap<String, Integer> funcAdds = new TreeMap<>(); //already read functions and their adresses
    private TreeMap<Integer, String> places = new TreeMap<>(); //places where we have to load the address of the function with a certain name
    private int returns = 0; //returns in function

    public CodeGenerationVisitor() {
        v = new Vector<>();
        pFunctions = new ArrayList<>();
        currentVars = new ArrayList<>();
    }

    public Instruction[] getProgram() {
        return program;
    }

    @Override
    public void visit(Program program) {
        try {
            //find main
            pFunctions = new ArrayList<>(Arrays.asList(program.getFunctions()));
            Stream<Function> pFunctionsStream = pFunctions.stream();
            Function main = pFunctionsStream.filter(f -> f.getName().equals("main")).findAny().orElse(null);
            if (main == null)
                throw new NoMainException();
            currentF = main;
            main.accept(this);
            v.add(new Halt());
            //generate code for all other functions after main
            for (Function f : program.getFunctions()) {
                if (!f.getName().equals("main")) {
                    currentF = f;
                    f.accept(this);
                }
            }
            //fill places before calls with addresses of according functions
            for (Map.Entry<Integer, String> entry : places.entrySet()) {
                int line = entry.getKey(); // place where the instruction is
                String fName = entry.getValue();//name of the function whose address should be written
                int add = funcAdds.get(fName);//find according address
                v.setElementAt(new Ldi(add), line); //add instruction
            }
            this.program = new Instruction[v.size()];
            this.program = v.toArray(this.program);
        }
        catch(NoMainException e){
            System.out.println(e);
        }
        catch (UnknownFunctionEx e){
            System.out.println(e);
        }
        catch (NoReturnException e){
            System.out.println(e);
        }
        catch (NotDeclVarException e){
            System.out.println(e);
        }
        catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    /* adds code for calling a function
    * if function is unknown throws exception*/
    public void visit(Call call) {
        //boolean readF = funcAdds.containsKey(call.getFunctionName());
        Stream<Function> pFunctionsStream = pFunctions.stream();
        Function ff = pFunctionsStream.filter(f->f.getName().equals(call.getFunctionName())).findAny().orElse(null);
        if(ff==null)
            throw new UnknownFunctionEx(call.getFunctionName());
        //if function was generated just add its parameters
        /*if(readF){
            for(String s : ff.getParameters()){
                currentVars.add(s);
            }
        }*/
        //add loading arguments
        for(Expression ex : call.getArguments())
            ex.accept(this);
        v.add(null);
        places.put(v.size()-1, call.getFunctionName());
        v.add(new asm.Call(call.getArguments().length));
    }

    @Override
    /*writes a function
    * thrown exception if function doesn't have a return statement*/
    public void visit(Function function) {
        returns--;
        funcAdds.put(function.getName(),v.size());//add function and it's adress to read functions
        for(String s : function.getParameters()){ //add parameters to current
            currentVars.add(s);
        }
        framePointer = function.getParameters().length-1;

        //add declarations
        for(Declaration d : function.getDeclarations()) {
            d.accept(this);
        }
        for(Statement s : function.getStatements()) {
            s.accept(this);
        }
        if(returns<0)
            throw new NoReturnException(function.getName());
        currentVars.clear();//clear variables when function ended
    }
    @Override
    /*adds return statement and if main just the expression*/
    public void visit(Return rreturn) {
        returns++;
        rreturn.getExpression().accept(this);
        if(!(currentF.getName().equals("main"))) {
            v.add(new asm.Return(currentF.getParameters().length+currentF.getDeclarations().length));
        }
    }

    @Override
    public void visit(Assignment assignment) {
        assignment.getExpression().accept(this);
        int index = currentVars.indexOf(assignment.getName());
        Instruction i2 = new Sts(index-framePointer);
        v.add(i2);
    }

    @Override
    public void visit(Binary binary) {
        binary.getRhs().accept(this);
        binary.getLhs().accept(this);
        switch (binary.getOperator()){
            case Minus:
                v.add(new Sub());
                break;
            case Plus:
                v.add(new Add());
                break;
            case Modulo:
                v.add(new Mod());
                break;
            case DivisionOperator:
                v.add(new Div());
                break;
            case MultiplicationOperator:
                v.add(new Mul());
                break;
        }
    }

    @Override
    public void visit(BinaryCondition binaryCondition) {
        binaryCondition.getRhs().accept(this);
        binaryCondition.getLhs().accept(this);
        switch(binaryCondition.getOperator()){
            case Or:
                v.add(new Or());
                break;
            case And:
                v.add(new And());
                break;
        }
    }



    @Override
    public void visit(Comparison comparison) {
        switch (comparison.getOperator()){
            case Equals:
                comparison.getRhs().accept(this);
                comparison.getLhs().accept(this);
                v.add(new Cmp(Op.Equals));
                break;
            case Less:
                comparison.getRhs().accept(this);
                comparison.getLhs().accept(this);
                v.add(new Cmp(Op.Less));
                break;
            case GreaterEqual:
                comparison.getRhs().accept(this);
                comparison.getLhs().accept(this);
                v.add(new Cmp(Op.Less));
                v.add(new Not());
                break;
            case NotEquals:
                comparison.getRhs().accept(this);
                comparison.getLhs().accept(this);
                v.add(new Cmp(Op.Equals));
                v.add(new Not());
                break;
            case LessEqual:
                comparison.getLhs().accept(this);
                comparison.getRhs().accept(this);
                v.add(new Cmp(Op.Less));
                v.add(new Not());
                break;
            case Greater:
                comparison.getLhs().accept(this);
                comparison.getRhs().accept(this);
                v.add(new Cmp(Op.Less));
                break;
        }
    }

    @Override
    public void visit(Composite composite) {
        for(Statement s : composite.getStatements())
            s.accept(this);
    }

    @Override
    public void visit(Declaration declaration) {
        v.add(new Decl(declaration.getNames().length));

        for(String s : declaration.getNames())
            currentVars.add(s);
    }

    @Override
    public void visit(ExpressionStatement expressionStatement) {
        expressionStatement.getExpression().accept(this);
        //deletes what's left after the expression unless it's write (write doesn't leave anything)
        if(!(expressionStatement.getExpression() instanceof Write))
            v.add(new Pop(Register.r0));
    }

    @Override
    public void visit(False ff) {
        v.add(new Ldi(0));
    }



    @Override
    public void visit(IfThenElse ifThenElse) {
        ifThenElse.getCond().accept(this);
        //if condition isn't true - jump to what's after then statement
        if((v.lastElement() instanceof Not))
            v.remove(v.size()-1);
        else
            v.add(new Not());
        v.add(null);
        branchHolder = v.size()-1;
        ifThenElse.getThenBranch().accept(this);
        v.setElementAt(new Brc(v.size()), branchHolder);
        //set address after then statement
        //if exists, add else statement
        if(ifThenElse.getElseBranch()!=null)
            ifThenElse.getElseBranch().accept(this);
    }

    @Override
    public void visit(Number number) {
        Instruction i1 = new Ldi(number.getValue());
        v.add(i1);
    }


    @Override
    public void visit(Read read) {
        v.add(new In());
    }



    @Override
    public void visit(True tt) {
        v.add(new Ldi(-1));
    }

    @Override
    public void visit(Unary unary) {
        switch (unary.getOperator()){
            case Minus:
                unary.getOperand().accept(this);
                v.add(new Ldi(0));
                v.add(new Sub());
                break;
        }
    }

    @Override
    public void visit(UnaryCondition unaryCondition) {
        switch (unaryCondition.getOperator()){
            case Not:
                unaryCondition.getOperand().accept(this);
                v.add(new Not());
                break;
        }
    }

    @Override
    /*throws exception is using undeclared variable (or not in params)*/
    public void visit(Variable variable) {
        int index = currentVars.indexOf(variable.getName());
        if(index == -1)
            throw new NotDeclVarException(variable.getName() + " in function " + currentF.getName());
        v.add(new Lfs(index-framePointer));
    }

    @Override
    public void visit(While ww) {
        if(ww.isDoWhile()){
            placeHolder = v.size();
            ww.getBody().accept(this);
            ww.getCond().accept(this);
            //if condition true jump back to body
            v.add(new Brc(placeHolder));
        }
        else {
            placeHolder = v.size();
            //if condition false jump to after body
            ww.getCond().accept(this);
            if((v.lastElement() instanceof Not))
                v.remove(v.size()-1);
            else
                v.add(new Not());
            v.add(null);
            branchHolder = v.size() - 1;
            ww.getBody().accept(this);
            v.add(new Ldi(-1));
            v.add(new Brc(placeHolder));
            v.setElementAt(new Brc(v.size()), branchHolder);
        }
    }

    @Override
    public void visit(Write write) {
        write.getExpression().accept(this);
        v.add(new Out());
    }



}

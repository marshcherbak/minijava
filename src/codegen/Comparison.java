package codegen;

public class Comparison extends Condition {
    private Expression lhs;
    private Comp operator;
    private Expression rhs;

    public Expression getLhs() {
        return lhs;
    }

    public Comp getOperator() {
        return operator;
    }

    public Expression getRhs() {
        return rhs;
    }

    public Comparison(Expression lhs, Comp operator, Expression rhs) {
        super();
        this.lhs = lhs;
        this.operator = operator;
        this.rhs = rhs;
    }

    public void accept(ProgramVisitor visitor){
        visitor.visit(this);
    }
}

package codegen;

import asm.Terminal;

public class NotDeclVarException extends CodeGenException {
    private String s;
    public NotDeclVarException(String s){
        super();
        this.s = s;
    }

    public String toString(){
        return super.toString() + Terminal.NEWLINE + "Undeclared variable " + s + " used."+Terminal.NEWLINE;
    }
}

package codegen;

public class Composite extends Statement{
    private Statement[] statements;
    public Statement[] getStatements(){
        return statements;
    }

    public Composite(Statement[] statements) {
        super();
        this.statements = statements;
    }

    public void accept(ProgramVisitor visitor){
        visitor.visit(this);
    }
}

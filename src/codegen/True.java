package codegen;

public class True extends Condition{
    public True() {
        super();
    }

    public void accept(ProgramVisitor visitor){
        visitor.visit(this);
    }
}

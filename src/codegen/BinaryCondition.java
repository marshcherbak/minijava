package codegen;

public class BinaryCondition extends Condition {
    private Condition lhs;
    private Bbinop operator;
    private Condition rhs;

    public Condition getLhs() {
        return lhs;
    }

    public Bbinop getOperator() {
        return operator;
    }

    public Condition getRhs() {
        return rhs;
    }

    public BinaryCondition(Condition lhs, Bbinop operator, Condition rhs) {
        super();
        this.lhs = lhs;
        this.operator = operator;
        this.rhs = rhs;
    }

    public void accept(ProgramVisitor visitor){
        visitor.visit(this);
    }
}

package codegen;

public class While extends Statement {
    private Condition cond;
    private Statement body;
    private boolean doWhile;
    public Condition getCond() {
        return cond;
    }

    public Statement getBody() {
        return body;
    }

    public While(Condition cond, Statement body, boolean doWhile) {
        super();
        this.cond = cond;
        this.body = body;
        this.doWhile = doWhile;
    }

    public boolean isDoWhile(){
        return doWhile;
    }

    public void accept(ProgramVisitor visitor){
        visitor.visit(this);
    }
}

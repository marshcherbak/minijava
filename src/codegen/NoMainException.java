package codegen;

import asm.Terminal;

public class NoMainException extends CodeGenException {
    public NoMainException(){
        super();
    }

    public String toString(){
        return super.toString() + Terminal.NEWLINE + "No main function."+Terminal.NEWLINE;
    }
}

package codegen;

import asm.Terminal;

public class UnknownFunctionEx extends CodeGenException {
    String s;
    public UnknownFunctionEx(String s){
        super();
        this.s = s;
    }

    public String toString(){
        return super.toString() + Terminal.NEWLINE + "Called function " + s + " is unknown (doesn't exist in the program)."
                + Terminal.NEWLINE;
    }
}

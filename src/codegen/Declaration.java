package codegen;

public class Declaration {
    private String[] names;

    public String[] getNames(){
        return names;
    }

    public Declaration(String[] names){
        this.names = names;
    }

    public void accept(ProgramVisitor visitor){
        visitor.visit(this);
    }
}

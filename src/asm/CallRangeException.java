package asm;

public class CallRangeException extends InterpreterException {

    String num;
    public CallRangeException(String msg) {
        super();
        num = msg;

    }

    public String toString(){
        return super.toString() + num + ": out of range. Invalid number of instruction address."+Terminal.NEWLINE;
    }
}

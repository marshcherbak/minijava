package asm;

public class CallReturnException extends InterpreterException {

    String s;
    public CallReturnException(String s) {
        super();
        this.s = s;
    }

    public String toString(){
        return super.toString() + s + " error. The number of arguments should be >= 0."+Terminal.NEWLINE;
    }
}

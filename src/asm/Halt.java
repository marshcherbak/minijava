package asm;

public class Halt extends Instruction {
    public void accept(AsmVisitor visitor) {
        visitor.visit(this);
    }
}

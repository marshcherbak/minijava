package asm;

public class Return extends Instruction {
    public void accept(AsmVisitor visitor) {
        visitor.visit(this);
    }

    private int n;

    public Return(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }
}

package asm;

public class AsmFormatVisitor implements AsmVisitor {

    private String formattedCode;
    private Instruction[] instructions;

    public String getFormattedCode() {
        try {
            if (instructions == null)
                throw new NullPointerException("null passed to AsmFormatVisitor");
            for (int i = 0; i < instructions.length; i++) {
                formattedCode += i + ": ";
                instructions[i].accept(this);
                formattedCode += Terminal.NEWLINE;
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        return formattedCode;
    }

    public AsmFormatVisitor(Instruction[] instructions) {
        this.formattedCode = "";
        this.instructions = instructions;
    }

    @Override
    public void visit(Nop nop) {
        formattedCode+= "NOP";
    }
    @Override
    public void visit(Add add) {
        formattedCode+= "ADD";
    }

    @Override
    public void visit(Sub sub) {
        formattedCode+="SUB";
    }

    @Override
    public void visit(Mul mul) {
        formattedCode+="MUL";
    }

    @Override
    public void visit(Mod mod) {
        formattedCode+="MOD";
    }

    @Override
    public void visit(Div div) {
        formattedCode+="DIV";
    }

    @Override
    public void visit(And and) {
        formattedCode+="AND";
    }

    @Override
    public void visit(Or or) {
        formattedCode+="OR";
    }

    @Override
    public void visit(Not not) {
        formattedCode+="NOT";
    }

    @Override
    public void visit(Ldi ldi) {
        formattedCode+="LDI " + ldi.getC();
    }

    @Override
    public void visit(Lfs lfs) {
        formattedCode+="LFS " + lfs.getI();
    }

    @Override
    public void visit(Sts sts) {
        formattedCode+="STS " + sts.getI();
    }

    @Override
    public void visit(Brc brc) {
        formattedCode+="BRC " + brc.getI();
    }

    @Override
    public void visit(Cmp cmp) {
        formattedCode+="CMP ";
        switch (cmp.getOp()){
            case Equals:
                formattedCode+="EQUALS";
                break;
            case Less:
                formattedCode+="LESS";
                break;
        }
    }

    @Override
    public void visit(Call call) {
        formattedCode+="CALL " + call.getN();
    }

    @Override
    public void visit(Decl decl) {
        formattedCode+="DECL " + decl.getN();
    }

    @Override
    public void visit(Return ret) {
        formattedCode+="RETURN " + ret.getN();
    }

    @Override
    public void visit(In in) {
        formattedCode+="IN";
    }

    @Override
    public void visit(Out out) {
        formattedCode+="OUT";
    }

    @Override
    public void visit(Push push) {
        formattedCode+="PUSH ";
        switch (push.getR()){
            case r0:
                formattedCode+="0";
                break;
            case r1:
                formattedCode+="1";
                break;
        }
    }

    @Override
    public void visit(Pop pop) {
        formattedCode+="POP ";
        switch (pop.getR()){
            case r0:
                formattedCode+="0";
                break;
            case r1:
                formattedCode+="1";
                break;
        }
    }

    @Override
    public void visit(Halt halt) {
        formattedCode+="HALT";
    }
}

package asm;

import java.util.*;
import java.util.stream.Stream;

public class Interpreter implements AsmVisitor {
    private Instruction[] program;
    private int[] intStack;
    private int stackPointer, framePointer;
    private int[] register;
    private int PC; //program counter

    public Interpreter(Instruction[] program) {
        this.program = program;
        register = new int[2];
        PC = 0;
        framePointer = -1;
        stackPointer = -1;
        intStack = new int[128];
    }

    public int execute(){
        try {
            if(program.length<2 || program==null)
                throw new NotEnoughInstrException();
            //check for halt instruction first
            ArrayList<Instruction> inst = new ArrayList<Instruction>(Arrays.asList(program));
            Stream<Instruction> stream = inst.stream();
            Instruction halt = stream.filter(i -> i instanceof Halt).findAny().orElse(null);
            if(halt == null)
                throw  new ProgramEndingException();
            //iterate over instructions
            while (PC < program.length) {
                program[PC].accept(this);
                PC++;
            }

        }
        catch (NotEnoughInstrException e){
            System.out.println(e);
            return -1;
        }
        catch(ProgramEndingException e){
            System.out.println(e);
            return -1;
        }
        catch (ArgumentsException a){
            System.out.println(a.toString());
            return -1;
        }
        catch(CallRangeException e){
            System.out.println(e.toString());
            return -1;
        }
        catch (Exception e){
            System.out.println(e.toString());
            return -1;
        }
        return intStack[stackPointer];
    }
    @Override
    public void visit(Nop nop) {
        PC++;
    }

    @Override
    public void visit(Add add) {
       if(stackPointer<1)
           throw new ArgumentsException("In method Add: "+Terminal.NEWLINE);
       //read
        int a = intStack[stackPointer];
        int b = intStack[stackPointer-1];
        //remove
        stackPointer-=2;
        int res = a + b;
        stackPointer++;
        //write res
        intStack[stackPointer]=res;

    }

    @Override
    public void visit(Sub sub) {
        if(stackPointer<1)
            throw new ArgumentsException("In method Add: "+Terminal.NEWLINE);
        int a = intStack[stackPointer];
        int b = intStack[stackPointer-1];
        stackPointer-=2;
        int res = a - b;
        stackPointer++;
        intStack[stackPointer]=res;
    }

    @Override
    public void visit(Mul mul) {
        if(stackPointer<1)
            throw new ArgumentsException("In method Add: "+Terminal.NEWLINE);
        int a = intStack[stackPointer];
        int b = intStack[stackPointer-1];
        stackPointer-=2;
        int res = a * b;
        stackPointer++;
        intStack[stackPointer]=res;
    }

    @Override
    public void visit(Mod mod) {
        if(stackPointer<1)
            throw new ArgumentsException("In method Add: "+Terminal.NEWLINE);
        int a = intStack[stackPointer];
        int b = intStack[stackPointer-1];
        stackPointer-=2;
        int res = a % b;
        stackPointer++;
        intStack[stackPointer]=res;
    }

    @Override
    public void visit(Div div) {
        if(stackPointer<1)
            throw new ArgumentsException("In method Add: "+Terminal.NEWLINE);
        int a = intStack[stackPointer];
        int b = intStack[stackPointer-1];
        stackPointer-=2;
        int res = a / b;
        stackPointer++;
        intStack[stackPointer]=res;
    }

    @Override
    public void visit(And and) {
        if(stackPointer<1)
            throw new ArgumentsException("In method Add: "+Terminal.NEWLINE);
        int a = intStack[stackPointer];
        int b = intStack[stackPointer-1];
        stackPointer-=2;
        int res = a & b;
        stackPointer++;
        intStack[stackPointer]=res;
    }

    @Override
    public void visit(Or or) {
        if(stackPointer<1)
            throw new ArgumentsException("In method Add: "+Terminal.NEWLINE);
        int a = intStack[stackPointer];
        int b = intStack[stackPointer-1];
        stackPointer-=2;
        int res = a | b;
        stackPointer++;
        intStack[stackPointer]=res;
    }

    @Override
    public void visit(Not not) {
        if(intStack[stackPointer] == -1)
            intStack[stackPointer]=0;
        else if(intStack[stackPointer] == 0)
            intStack[stackPointer] = -1;
    }

    @Override
    public void visit(Ldi ldi) {
        stackPointer++;
        intStack[stackPointer] = ldi.getC();
    }

    @Override
    public void visit(Lfs lfs) {
        stackPointer++;
        intStack[stackPointer]=intStack[lfs.getI()+framePointer];
    }

    @Override
    public void visit(Sts sts) {
        intStack[sts.getI()+framePointer] = intStack[stackPointer];
        stackPointer--;
    }

    @Override
    /* throws exception if instruction address is invalid
    * sets PC to needed instruction*/
    public void visit(Brc brc) {
        if(intStack[stackPointer] == -1){
            PC = brc.getI()-1;
            if(PC>program.length-1)
                throw new CallRangeException("In Brc "+brc.getI());
        }
        stackPointer--;
    }

    @Override
    public void visit(Cmp cmp) {
        if(stackPointer<1)
            throw new ArgumentsException("In method CMP: "+Terminal.NEWLINE);
        int a = intStack[stackPointer];
        int b = intStack[stackPointer-1];
        stackPointer-=1;
        switch (cmp.getOp()){
            case Less:
                if(a < b)
                    intStack[stackPointer] = -1;
                else
                    intStack[stackPointer] = 0;
                break;
            case Equals:
                if(a == b)
                    intStack[stackPointer] = -1;
                else
                    intStack[stackPointer] = 0;
                break;
        }
    }

    @Override
    /*
    * throws exception if num of args < 0 or more than elements in stack
    * sets PC to the adress of the called function
    * before params writes framepointer and command where we return*/
    public void visit(Call call) {
        if(call.getN()<0)
            throw new CallReturnException("In method Call " + call.getN());
        if(call.getN()>stackPointer)
            throw new ArgumentsException("In method Call " + call.getN());
        int ad = intStack[stackPointer];
        if(ad > program.length-1 || ad < 0)
            throw new CallRangeException("In call "+call.getN());
        stackPointer--;
        for(int i = 0; i<call.getN(); i++){
            intStack[stackPointer+2-i] = intStack[stackPointer-i];
        }
        int k = stackPointer+(2-call.getN());
        intStack[k] = framePointer;
        intStack[k-1] = PC;
        stackPointer+=2;
        framePointer = stackPointer;
        PC = ad-1;
    }

    @Override
    public void visit(Decl decl) {
        for(int k = 1; k <= decl.getN(); k++){
            stackPointer++;
        }


    }

    @Override
    /*checks return for valid n
    * returns to stack before call + res of the function
    * doesn't check if return has the correct n in relation to how many variables are declared + params*/
    public void visit(Return ret) {
        if(ret.getN()<0 || ret.getN()>stackPointer)
            throw new CallReturnException("In method Return: "+Terminal.NEWLINE);
        int r = intStack[stackPointer];
        stackPointer--;
        for(int i = 0; i < ret.getN(); i++)
            stackPointer--;

        framePointer = intStack[stackPointer];

        stackPointer--;

        PC = intStack[stackPointer];

        intStack[stackPointer] = r;
    }

    @Override
    public void visit(In in) {
        int a = Terminal.askInt("Enter int: ");
        stackPointer++;
        intStack[stackPointer]=a;
    }

    @Override
    public void visit(Out out) {
        System.out.println(intStack[stackPointer]);
        stackPointer--;
    }

    @Override
    public void visit(Push push) {
        switch (push.getR()){
            case r1:
                stackPointer++;
                intStack[stackPointer] = register[1];
                break;
            case r0:
                stackPointer++;
                intStack[stackPointer] = register[0];
                break;
        }
    }

    @Override
    public void visit(Pop pop) {
        switch (pop.getR()){
            case r1:
                register[1] = intStack[stackPointer];
                stackPointer--;
                break;
            case r0:
                register[0] = intStack[stackPointer];
                stackPointer--;
                break;
        }
    }

    @Override
    public void visit(Halt halt) {
        PC = program.length;
    }
}

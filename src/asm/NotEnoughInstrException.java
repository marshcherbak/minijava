package asm;

public class NotEnoughInstrException extends InterpreterException {
    public NotEnoughInstrException() {
        super();
    }

    public String toString(){
        return super.toString() + " Not enough instructions for a program."+Terminal.NEWLINE;
    }
}

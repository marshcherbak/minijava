package asm;

public class Cmp extends Instruction {
    public void accept(AsmVisitor visitor) {
        visitor.visit(this);
    }

    private Op op;

    public Cmp(Op op) {
        this.op = op;
    }

    public Op getOp() {
        return op;
    }
}


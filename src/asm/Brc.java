package asm;

public class Brc extends Instruction {
    public void accept(AsmVisitor visitor) {
        visitor.visit(this);
    }

    private int i;

    public Brc(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }
}

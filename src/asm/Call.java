package asm;

public class Call extends Instruction {
    public void accept(AsmVisitor visitor) {
        visitor.visit(this);
    }

    private int n;

    public Call(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }
}

package asm;

public class Lfs extends Instruction {
    public void accept(AsmVisitor visitor) {
        visitor.visit(this);
    }

    private int i;

    public Lfs(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }
}

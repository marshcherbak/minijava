package asm;

public class Sts extends Instruction {
    public void accept(AsmVisitor visitor) {
        visitor.visit(this);
    }
    private int i;

    public Sts(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }
}

package asm;

public class InterpreterTest {
    public static void test(Instruction[] ins){
        AsmFormatVisitor af = new AsmFormatVisitor(ins);
        System.out.println(af.getFormattedCode());
        Interpreter i = new Interpreter(ins);
        i.execute();

    }
    public static void main(String[] args) {
        System.out.println(Terminal.NEWLINE+ "Fak function" + Terminal.NEWLINE);
        Instruction[] p3 = { new In(), new Ldi(6), new Call(1), new Out(), new Ldi(0), new Halt(), new Ldi(1),
                new Lfs(0), new Cmp(Op.Equals), new Not(), new Brc(13), new Ldi(1), new Return(1), new Ldi(1),
                new Lfs(0), new Sub(), new Ldi(6), new Call(1), new Lfs(0), new Mul(), new Return(1)};

        test(p3);


        System.out.println(Terminal.NEWLINE+ "GGT function" + Terminal.NEWLINE);
        Instruction[] pr4 = { new In(), new In(), new Ldi(7), new Call(2), new Out(), new Ldi(0),
                new Halt(), new Decl(1), new Lfs(-1), new Lfs(0), new Cmp(Op.Less), new Brc(16),
                new Lfs(0), new Lfs(-1), new Sts(0), new Sts(-1), new Lfs(0), new Sts(1),
                new Lfs(0), new Lfs(-1), new Mod(), new Sts(0), new Lfs(1), new Sts(-1), new Ldi(0), new Lfs(0),
                new Cmp(Op.Equals), new Not(), new Brc(16), new Lfs(-1), new Return(3)};

        test(pr4);

        System.out.println(Terminal.NEWLINE+ "Class example function" + Terminal.NEWLINE);
        Instruction[] pr2 = { new Decl(3), new In(), new Sts(2), new Ldi(0), new
                Sts(3), new Ldi(1), new Sts(1), new Lfs(2), new Lfs(1),
        new Cmp(Op.Equals), new Brc(21), new Lfs(1), new Lfs(3), new Add(),
        new Sts(3), new Ldi(1), new Lfs(1), new Add(), new Sts(1), new Ldi(-1),
        new Brc(7), new Lfs(3), new Out(), new Ldi(0), new Halt()};

        test(pr2);

        /*
         * FUnctions that can come to exceptions
         */
        Instruction[] pr5 = { new In(), new In(), new Ldi(7), new Call(5), new Out(), new Ldi(0),
                new Halt(), new Decl(1), new Lfs(-1), new Lfs(0), new Cmp(Op.Less), new Brc(58),
                new Lfs(0), new Lfs(-1), new Sts(0), new Sts(-1), new Lfs(0), new Sts(1),
                new Lfs(0), new Lfs(-1), new Mod(), new Sts(0), new Lfs(1), new Sts(-1), new Ldi(0), new Lfs(0),
                new Cmp(Op.Equals), new Not(), new Brc(16), new Lfs(-1), new Return(3)};

        test(pr5);

        Instruction[] pr6 = { new In(), new In(), new Ldi(7), new Call(2), new Out(), new Ldi(0),
                new Halt(), new Decl(1), new Lfs(-1), new Lfs(0), new Cmp(Op.Less), new Brc(55),
                new Lfs(0), new Lfs(-1), new Sts(0), new Sts(-1), new Lfs(0), new Sts(1),
                new Lfs(0), new Lfs(-1), new Mod(), new Sts(0), new Lfs(1), new Sts(-1), new Ldi(0), new Lfs(0),
                new Cmp(Op.Equals), new Not(), new Brc(48), new Lfs(-1), new Return(3)};


        test(pr6);
        Instruction[] p7 = { new In(), new Ldi(6), new Call(1), new Out(), new Ldi(0), new Ldi(1),
                new Lfs(0), new Cmp(Op.Equals), new Not(), new Brc(13), new Ldi(1), new Return(1), new Ldi(1),
                new Lfs(0), new Sub(), new Ldi(6), new Call(1), new Lfs(0), new Mul(), new Return(1)};

        test(p7);

    }
}

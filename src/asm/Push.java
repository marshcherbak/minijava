package asm;

public class Push extends Instruction {
    public void accept(AsmVisitor visitor) {
        visitor.visit(this);
    }

    private Register r;

    public Push(Register r) {
        this.r = r;
    }

    public Register getR() {
        return r;
    }
}

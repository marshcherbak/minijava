package asm;

public class ProgramEndingException extends InterpreterException {
    public ProgramEndingException() {
        super();
    }

    public String toString(){
        return super.toString() + "  No Halt instruction." + Terminal.NEWLINE;
    }
}

package asm;

public class Pop extends Instruction {
    public void accept(AsmVisitor visitor) {
        visitor.visit(this);
    }

    private Register r;

    public Pop(Register r) {
        this.r = r;
    }

    public Register getR() {
        return r;
    }
}

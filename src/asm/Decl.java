package asm;

public class Decl extends Instruction {
    public void accept(AsmVisitor visitor) {
        visitor.visit(this);
    }

    private int n;

    public Decl(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }
}

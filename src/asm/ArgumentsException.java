package asm;

public class ArgumentsException extends InterpreterException {

    String s;
    public ArgumentsException(String s) {
        super();
        this.s = s;
    }

    @Override
    public String toString(){
        return super.toString() + s + "  Not enough arguments for this command."+Terminal.NEWLINE;
    }
}

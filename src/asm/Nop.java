package asm;

public class Nop extends Instruction {
    public void accept(AsmVisitor visitor) {
        visitor.visit(this);
    }
}
